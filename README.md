# Fatty

Android Project for "Mobile Computing and Applications" of Postgraduate Programme "Digital Systems & Services", 
Department of Digital Systems, University of Piraeus.        

__Developer__: Marios Touloupou, touloupos@gmail.com   

## Application Specifications

__App Name__: Fatty
 
__App Description__: "Fatty" is a recipe cookbook mobile application for Android phones. Just fill your desired ingredient/s and
and you will come across a huge list of relevant cook recipes to choose from. Easy and tasty :)

__Key Features__:    
*  User login / register system      
*  Account Modification (Change mail/pass/etc)
*  Search Recipes (retrieve from Edamam API)
*  Add recipes to "Favorites"


## Getting started

### Running the app:
*  Install  the latest Android Studio on your computer - [Install Android Studio](https://developer.android.com/studio/install)
*  Download the project from Bitbucket - [Fatty on Bitbucket](xxxxxxxxxx)
*  Import the project to Android Studio
*  Run the app on Android devices or with an emulator

### Components:
* ``LoginActivity , SignupActivity, ResetPasswordActivity, MyFavoritesActivity, MainActivity, HomeActivity`` 

### API

Fatty uses the Edamam API, which is available [here](https://developer.edamam.com/) and specifically the 'Recipe Search API'

### Data Model

__Firebase Authentication__:    

Fatty uses Firebase Authetication in order to know the identity of a user. Knowing a user's identity allows the app 
to securely save user data in the cloud and provide the same personalized experience across all of the user's devices.
     
__Key capabilities__:  
Email and password based authentication     

__Firestore Database__:    

Cloud Firestore is a flexible, scalable database for mobile, web, and server development from Firebase and Google Cloud Platform. 
Firestore keeps the data in sync across client apps through realtime listeners and offers offline support.

Fatty uses the `fatty-c43f8` database with the collection 'favorites'. 
The 'favorites' collection includes documents each one with a unique identifier. Each document is linked to a specific userbase on
user_uuid that is retrieved by the firebase authentication session.

### Contributing

You may contribute to this repository similar to other (sub-) projects, i.e. by creating pull requests.    


#### Lead Developer

The following developer is responsible for this repository and have admin rights. He can, for example, merge pull requests.    
*  Marios Touloupou (@mtouloup)


