package unipi.me1751.fatty.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import unipi.me1751.fatty.model.RecipeResponse;

/**
 * Created by Marios on 5/12/2018.
 */

public interface RecipeApiService {

    @GET("/search")
    Call<RecipeResponse> getRecipes(
            @Query("q") String q,
            @Query("app_id") String app_id,
            @Query("app_key") String app_key,
            @Query("from")int from ,
            @Query("to") int to
    );

}
