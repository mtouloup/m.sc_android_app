package unipi.me1751.fatty;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import unipi.me1751.fatty.activity.RecipesActivity;
import unipi.me1751.fatty.adapter.FavoritesListAdapter;
import unipi.me1751.fatty.model.Favorites;

public class My_favorites extends AppCompatActivity {

    private Toolbar mActionBarToolbar;
    private FirebaseAuth auth;
    private RecyclerView favoritesList;
    private FirebaseFirestore mFirestore;
    private static final String TAG = "Message";
    private FavoritesListAdapter favoritesListAdapter;
    private List<Favorites> listFavorites;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorites);

        listFavorites = new ArrayList<>();
        favoritesListAdapter = new FavoritesListAdapter(listFavorites);
        setSupportActionBar((Toolbar) findViewById(R.id.favorites_toolbar));

        auth = FirebaseAuth.getInstance();
        favoritesList = (RecyclerView) findViewById(R.id.list_favorites);
        favoritesList.setHasFixedSize(true);
        favoritesList.setLayoutManager(new LinearLayoutManager(this));
        favoritesList.setAdapter(favoritesListAdapter);
        mFirestore = FirebaseFirestore.getInstance();
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        String uuid = currentFirebaseUser.getUid();


        mFirestore.collection("favorites").whereEqualTo("user_uuid",uuid).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {

                if (e != null)
                {
                    Log.d(TAG, "Error:" + e.getMessage());
                }

                for (DocumentChange doc: documentSnapshots.getDocumentChanges()){

                    if (doc.getType() == DocumentChange.Type.ADDED){
                        Favorites favorite = doc.getDocument().toObject(Favorites.class);

                        Log.d(TAG, favorite.getLabel());
                        listFavorites.add(favorite);

                        favoritesListAdapter.notifyDataSetChanged();
                    }


                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.main_menu, menu);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.account:
                Intent intent2 = new Intent(this, MainActivity.class);
                this.startActivity(intent2);
                break;
            case R.id.acc_logout:
                auth.signOut();
                Intent intent3 = new Intent(this, LoginActivity.class);
                this.startActivity(intent3);
                finish();
                break;
            case R.id.my_favorites:
                Intent intent4 = new Intent(this, My_favorites.class);
                this.startActivity(intent4);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

//
//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(My_favorites.this, HomeActivity.class);
//        startActivity(intent);
//        finish();
//    }

}
