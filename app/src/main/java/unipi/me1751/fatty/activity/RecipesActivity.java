package unipi.me1751.fatty.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import unipi.me1751.fatty.HomeActivity;
import unipi.me1751.fatty.LoginActivity;
import unipi.me1751.fatty.MainActivity;
import unipi.me1751.fatty.My_favorites;
import unipi.me1751.fatty.R;
import unipi.me1751.fatty.adapter.RecipesAdapter;
import unipi.me1751.fatty.model.Recipe;
import unipi.me1751.fatty.model.RecipeResponse;
import unipi.me1751.fatty.rest.RecipeApiService;

public class RecipesActivity extends AppCompatActivity{

    private static final String TAG = RecipesActivity.class.getSimpleName();
    public static final String BASE_URL = "https://api.edamam.com/";
    private static Retrofit retrofit = null;
    private RecyclerView recyclerView = null;
    private final static String APP_KEY = "17b9a8581cf681dfd921af3b479213ef";
    private final static String APP_ID = "a1c024e9";
    private String keyword="";
    private int from = 0;
    private int to = 50;
    private Toolbar mActionBarToolbar;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    private LinearLayout linlaHeaderProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);



        auth = FirebaseAuth.getInstance();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        setSupportActionBar((Toolbar) findViewById(R.id.recipe_toolbar));


        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                keyword= null;
            } else {
                keyword= extras.getString("keyword");
            }
        } else {
            keyword= (String) savedInstanceState.getSerializable("keyword");
        }
        connectAndGetApiData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.main_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.account:
                Intent intent2 = new Intent(this, MainActivity.class);
                this.startActivity(intent2);
                break;
            case R.id.acc_logout:
                auth.signOut();
                Intent intent3 = new Intent(this, LoginActivity.class);
                this.startActivity(intent3);
                finish();
                break;
            case R.id.my_favorites:
                Intent intent4 = new Intent(this, My_favorites.class);
                this.startActivity(intent4);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }



    // This method create an instance of Retrofit
    // set the base url
    public void connectAndGetApiData(){

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        linlaHeaderProgress.setVisibility(View.VISIBLE);

        RecipeApiService recipeApiService = retrofit.create(RecipeApiService.class);
        Call<RecipeResponse> call = recipeApiService.getRecipes(keyword,APP_ID,APP_KEY,from,to);

        call.enqueue(new Callback<RecipeResponse>() {
            @Override
            public void onResponse(Call<RecipeResponse> call, Response<RecipeResponse> response) {

                List<Recipe> recipes= response.body().getResults();
                recyclerView.setAdapter(new RecipesAdapter(recipes,R.layout.list_item_recipe,getApplicationContext()));
                linlaHeaderProgress.setVisibility(View.GONE);

                if (recipes.size() == 0)
                {
                    Toast.makeText(RecipesActivity.this,
                            "No recipes Found, check with another keyword!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RecipesActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();

                }else
                {

                    Toast.makeText(RecipesActivity.this,
                            "Number of recipes received: " + recipes.size(), Toast.LENGTH_LONG).show();
                }

                Log.d(TAG, "Number of recipes received: " + recipes.size());

            }

            @Override
            public void onFailure(Call<RecipeResponse> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RecipesActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

}
