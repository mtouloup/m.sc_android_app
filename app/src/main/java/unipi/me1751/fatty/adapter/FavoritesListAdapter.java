package unipi.me1751.fatty.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import unipi.me1751.fatty.R;
import unipi.me1751.fatty.model.Favorites;

/**
 * Created by Marios on 5/23/2018.
 */

public class FavoritesListAdapter extends RecyclerView.Adapter<FavoritesListAdapter.ViewHolder> {

    public List<Favorites> favoritesList;
    private FirebaseFirestore db;

    public FavoritesListAdapter(List<Favorites> favoritesList){

        this.favoritesList = favoritesList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_favorite_items,parent,false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.labelText.setText(favoritesList.get(position).getLabel());
        holder.sourceText.setText(favoritesList.get(position).getSource());


        holder.del_fav.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                CollectionReference mFirestore = FirebaseFirestore.getInstance().collection("favorites");
                mFirestore.document(favoritesList.get(position).getDocument_uuid()).delete();

                Intent intent = ((Activity) v.getContext()).getIntent();
                ((Activity) v.getContext()).finish();
                ((Activity) v.getContext()).startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return favoritesList.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {

        View mView;

        public TextView labelText;
        public TextView sourceText;
        public ImageButton del_fav;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            labelText = (TextView) mView.findViewById(R.id.favorite_label);
            sourceText = (TextView) mView.findViewById(R.id.favorite_source);
            del_fav = (ImageButton) mView.findViewById(R.id.del_favorite);
        }
    }
}
