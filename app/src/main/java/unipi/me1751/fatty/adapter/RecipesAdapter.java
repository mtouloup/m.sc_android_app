package unipi.me1751.fatty.adapter;

import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.EventLog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import unipi.me1751.fatty.R;
import unipi.me1751.fatty.activity.RecipesActivity;
import unipi.me1751.fatty.model.Favorites;
import unipi.me1751.fatty.model.Recipe;

/**
 * Created by Marios on 5/12/2018.
 */
public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.RecipeViewHolder> {

    private List<Recipe> recipes;
    private int rowLayout;
    private Context context;
    public static final String IMAGE_URL_BASE_PATH="https://www.edamam.com/web-img/";
    private DatabaseReference mFavouriteReference;

    FirebaseFirestore db;

    public RecipesAdapter(List<Recipe> recipes, int rowLayout, Context context) {

        this.recipes = recipes;
        this.rowLayout = rowLayout;
        this.context = context;

    }

    //A view holder inner class where we get reference to the views in the layout using their ID
    public static class RecipeViewHolder extends RecyclerView.ViewHolder {

        LinearLayout recipesLayout;
        ImageView image_url;
        TextView source;
        TextView calories;
        TextView recipe_label;
        TextView ingredients;
        final ImageButton favorite;


        public RecipeViewHolder(View v) {

            super(v);
            recipesLayout = (LinearLayout) v.findViewById(R.id.recipes_layout);
            image_url = (ImageView) v.findViewById(R.id.recipe_image);
            source = (TextView) v.findViewById(R.id.source);
            calories = (TextView) v.findViewById(R.id.calories);
            recipe_label = (TextView) v.findViewById(R.id.label);
            ingredients = (TextView) v.findViewById(R.id.ingredients);
            favorite = (ImageButton) v.findViewById(R.id.favorites);
        }

    }

    @Override
    public RecipesAdapter.RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RecipeViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final RecipeViewHolder holder, final int position) {


        String image_url = recipes.get(position).getRecipeDetails().getImage_url();
        Log.d("image url", image_url);
        Picasso.with(context)
                .load(image_url)
                .placeholder(android.R.drawable.sym_def_app_icon)
                .error(android.R.drawable.sym_def_app_icon)
                .into(holder.image_url);
        DecimalFormat df = new DecimalFormat("#");

        holder.source.setText("By " + recipes.get(position).getRecipeDetails().getSource());
        holder.recipe_label.setText(recipes.get(position).getRecipeDetails().getLabel());
        holder.calories.setText(df.format(Double.valueOf(recipes.get(position).getRecipeDetails().getCalories())) + " CALORIES");
        holder.ingredients.setText(recipes.get(position).getRecipeDetails().getIngredientsSize() + " INGREDIENTS");
        holder.image_url.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                browserIntent.setData(Uri.parse(recipes.get(position).getRecipeDetails().getPreparation()));
                context.startActivity(browserIntent);
            }
        });



//        /** get current user uuid **/
//        db = FirebaseFirestore.getInstance();
//        final FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
//        db.collection("favorites").document().addSnapshotListener(new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
//                CollectionReference favorite_items = db.collection("favorites");
//                favorite_items.whereEqualTo("label", recipes.get(position).getRecipeDetails().getLabel())
//                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
//                            @Override
//                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
//                                if (e != null)
//                                {
//                                    Log.d("ERROR", "Error:" + e.getMessage());
//                                }
//                                for (DocumentSnapshot doc: documentSnapshots.getDocuments()){
//                                    holder.favorite.setImageResource(R.drawable.heart_active);
//                                }
//
//                            }
//                        });
//            }
//
//        });


        holder.favorite.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                addFavorite(recipes.get(position).getRecipeDetails().getLabel(), recipes.get(position).getRecipeDetails().getSource());
                Toast.makeText(v.getContext(), "Added to favorites!", Toast.LENGTH_SHORT).show();
                holder.favorite.setImageResource(R.drawable.heart_active);
                v.setEnabled(false);
            }
        });

    }


    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public void addFavorite(String label, String url)
    {
        Log.w("PRESSED","Favorites Clicked");

        //DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        //mFavouriteReference = FirebaseDatabase.getInstance().getReference("favourite");
        //mFavouriteReference.child(currentFirebaseUser.getUid()).child(label).setValue(url);

        db = FirebaseFirestore.getInstance();
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;

        DocumentReference ref = db.collection("favorites").document();
        String myId = ref.getId();

        Map<String,Object> taskMap = new HashMap<>();
        taskMap.put("label", label);
        taskMap.put("url", "By "+url);
        taskMap.put("user_uuid",currentFirebaseUser.getUid());
        taskMap.put("document_uuid", myId);

        db.collection("favorites").document(myId).set(taskMap);

    }

}