package unipi.me1751.fatty.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marios on 5/14/2018.
 */

public class Ingredient {
    @SerializedName("text")
    private String text;


    @SerializedName("weight")
    private String weight;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
