package unipi.me1751.fatty.model;

/**
 * Created by Marios on 5/23/2018.
 */

public class Favorites {

    String label, url, user_uuid, document_uuid;

    public Favorites(){

    }


    public String getDocument_uuid() {
        return document_uuid;
    }

    public void setDocument_uuid(String document_uuid) {
        this.document_uuid = document_uuid;
    }

    public Favorites(String label, String source, String user_uuid, String document_uuid){
        this.label = label;
        this.url  = source;
        this.user_uuid = user_uuid;
        this.document_uuid = document_uuid;

    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSource() {
        return url;
    }

    public void setSource(String source) {
        this.url = source;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }
}

