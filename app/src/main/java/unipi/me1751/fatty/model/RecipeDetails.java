package unipi.me1751.fatty.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marios on 5/14/2018.
 */

public class RecipeDetails {

    @SerializedName("label")
    private String label;
    @SerializedName("image")
    private String image_url;
    @SerializedName("source")
    private String source;
    @SerializedName("url")
    private String preparation;
    @SerializedName("calories")
    private String calories;
    @SerializedName("ingredients")
    private List<Ingredient> ingredients = new ArrayList<Ingredient>();

    public RecipeDetails (String label, String image_url, String source, String preparation, String calories) {

        this.label = label;
        this.image_url = image_url;
        this.source = source;
        this. preparation = preparation;
        this.calories = calories;
        this.ingredients = ingredients;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getImage_url() {
        return image_url;
    }
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
    public String getPreparation() {
        return preparation;
    }
    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }
    public String getCalories() {
        return calories;
    }
    public void setCalories(String calories) {
        this.calories = calories;
    }
    public List<Ingredient> getIngredients() {
        return ingredients;
    }
    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
    public String getIngredientsSize() {
        return String.valueOf(this.ingredients.size());
    }

}
