package unipi.me1751.fatty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Marios on 5/12/2018.
 */

public class RecipeResponse {

    @SerializedName("from")
    private int from;
    @SerializedName("to")
    private int to;
    @SerializedName("hits")
    private List<Recipe> results;

    public List<Recipe> getResults() {
        return results;
    }
    public void setResults(List<Recipe> results) {
        this.results = results;
    }
    public int getFrom() {
        return from;
    }
    public void setFrom(int from) {
        this.from = from;
    }
    public int getTo() {
        return to;
    }
    public void setTo(int to) {
        this.to = to;
    }

}


