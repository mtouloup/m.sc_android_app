package unipi.me1751.fatty.model;


import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marios on 5/12/2018.
 */

public class Recipe {

    @SerializedName("recipe")
    private RecipeDetails recipeDetails;

    public RecipeDetails getRecipeDetails() {
        return recipeDetails;
    }

    public void setRecipeDetails(RecipeDetails recipeDetails) {
        this.recipeDetails = recipeDetails;
    }



}