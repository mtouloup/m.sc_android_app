package unipi.me1751.fatty;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import retrofit2.Retrofit;
import unipi.me1751.fatty.activity.RecipesActivity;

public class HomeActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    private Button btn_search;
    private EditText recipe_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_home);

        btn_search = (Button) findViewById(R.id.search_keyword);
        recipe_search = (EditText) findViewById(R.id.recipe_search);

        auth = FirebaseAuth.getInstance();

        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(mToolBar);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String keyword = recipe_search.getText().toString();
                Intent intent = new Intent(HomeActivity.this, RecipesActivity.class);

                intent.putExtra("keyword", keyword);
                startActivity(intent);
                //finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.main_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.account:
                Intent intent2 = new Intent(this, MainActivity.class);
                this.startActivity(intent2);
                break;
            case R.id.acc_logout:
                auth.signOut();
                Intent intent3 = new Intent(this, LoginActivity.class);
                this.startActivity(intent3);
                finish();
                break;
            case R.id.my_favorites:
                Intent intent4 = new Intent(this, My_favorites.class);
                this.startActivity(intent4);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

}


